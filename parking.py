from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np, imutils, time, cv2, mysql.connector as conn


def main():
    print("Parking code is running!!")
    protxt = "models/MobileNetSSD_deploy.prototxt.txt"
    model = "models/MobileNetSSD_deploy.caffemodel"

    CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow",
               "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]
    COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

    print("[INFO] loading model...")
    net = cv2.dnn.readNetFromCaffe(protxt, model)

    print("[INFO] starting video stream...")
    vs = VideoStream(src="rtsp://admin:india123@192.168.1.10:554/Streaming/channels/1/").start()
    time.sleep(2.0)
    fps = FPS().start()
    ret = 0
    while True:
        frame = vs.read()
        frame = imutils.resize(frame, width=800)

        (h, w) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)

        net.setInput(blob)
        detections = net.forward()

        for i in np.arange(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > 0.2:
                idx = int(detections[0, 0, i, 1])
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                if "person" in CLASSES[idx]:
                    label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
                    cv2.rectangle(frame, (startX, startY), (endX, endY), COLORS[idx], 2)
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    cv2.putText(frame, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                    ret, level = cv2.connectedComponents(cv2.cvtColor(frame.astype(np.uint8), cv2.COLOR_BGR2GRAY))
        occupancy = database(1, 0)
        ret -= 1
        if ret > occupancy:
            if ret == occupancy:
                pass
            else:
                # print("Present Count -", ret)
                database(0, ret)
        else:
            if occupancy == 0:
                pass
            elif ret < occupancy:
                # print("Count after removing -", ret)
                database(0, ret)
            else: pass
        cv2.imshow("Parking", frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break
        fps.update()

    fps.stop()
    # print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
    # print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
    cv2.destroyAllWindows()
    vs.stop()


def database(flag, ret):
    con = conn.connect(host="localhost", db="smart_parking_system", user="root", passwd="")
    cursor = con.cursor(buffered=True)
    if flag == 1:
        cursor.execute("SELECT `no_of_cars` FROM `parking` WHERE `id` = 1")
        result = cursor.fetchone()
        return result[0]
    else:
        cursor.execute("UPDATE `parking` SET `no_of_cars`= {} WHERE `id` = 1".format(ret))
        # print("Record Updated!!")
    con.commit()
    con.close()


main()