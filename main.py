from flask import Flask, request, render_template
from entry import main as em
from exit import main as exm
import qrcode, random, os, mysql.connector as conn, json

app = Flask(__name__)


con = conn.connect(host="localhost", db="smart_parking_system", user="root", passwd="")
cursor = con.cursor(buffered=True)


@app.route("/")
def index():
    try:
        files = os.listdir("static/")
        for f in files:
            if "myqr" in f:
                # print(f)
                os.remove("static/"+f)
    except:
        pass
    id = random.randint(3, 1000)
    data = "Welcome to C21 Mall. Random ID - {}".format(id)
    # print(data)
    filename = "static/myqr{}.png".format(id)
    img = qrcode.make(data)
    img.save(filename)
    return render_template('index.html', qr_image=filename)


@app.route("/registration", methods=['POST'])
def registration():
    if request.method == 'POST':
        data = request.get_json()
        query = "INSERT INTO `users`(`id`, `fname`, `lname`, `email`, `phone`, `passwd`) " \
                "VALUES (NULL, '{}', '{}', '{}', '{}', '{}')".format(data['fname'], data['lname'], data['email'], data['phone'], data['password'])
        cursor.execute(query)
        con.commit()
        return json.dumps({"message": "Record inserted successfully!!"})


@app.route("/login", methods=['POST'])
def login():
    if request.method == 'POST':
        data = request.get_json()
        try:
            query = "SELECT id, passwd FROM users WHERE email = '{}';".format(data['email'])
            cursor.execute(query)
            result = cursor.fetchone()
            con.commit()
            if data['password'] == result[1]:
                return json.dumps({"message": "Login successful!!", 'user_id': result[0]})
            else:
                return json.dumps({"message": "Invalid Password. Please check!!"})
        except:
            return json.dumps({"message": "Invalid Email. Please check!!"})


@app.route("/entry", methods=['POST'])
def entry():
    if request.method == 'POST':
        data = request.get_json()
        plate = em(1, con, cursor, data['user_id'])
        return json.dumps({'message': "Success Entry!!", 'licence_plate': plate})


@app.route("/exit", methods=['POST'])
def exit():
    if request.method == 'POST':
        data = request.get_json()
        exm(1, con, cursor, data['user_id'])
        return json.dumps({'message': "Success Exit!!"})


@app.route('/parking')
def database():
    cursor.execute("SELECT `no_of_cars` FROM `parking` WHERE `id` = 1")
    result = cursor.fetchone()
    return render_template('parking.html', park_count=result[0])


if __name__ == "__main__":
    from werkzeug.serving import run_simple
    run_simple('localhost', 5000, app)
    # app.run(host='localhost', port=5000, debug=True)