import cv2, imutils, pickle, numpy as np
from skimage.transform import resize
from skimage.filters import threshold_otsu


def main(flag, con, cursor, user_id):
    if flag == 1:
        try:
            # video_capture = cv2.VideoCapture("rtsp://admin:india123@192.168.1.13:554/Streaming/channels/1/")
            video_capture = cv2.VideoCapture("C:\\Users\\vcool\\Downloads\\cars\\1.jpg")
            plate_string, c_plate_string = "", ""
            ret, frame = video_capture.read()
            if ret:
                # resize = imutils.resize(frame, width=500)
                # cv2.imshow("Initial", resize)
                # cv2.waitKey(0)
                image = cv2.cvtColor(cv2.cvtColor(frame.astype(np.uint8), cv2.COLOR_BGR2RGB), cv2.COLOR_RGB2BGR)
                # cv2.imwrite("static/entry.jpg", image)
                plate_string = detect_license_plate(image)
                # print("Entry point Original License Plate -", plate_string)
                if len(plate_string) < 10:
                    pass
                else:
                    c_plate_string = plate_string[:2] + string_correction(plate_string[2:])
                    print("Corrected License Plate - " + c_plate_string)
                    database(plate_string[:2] + string_correction(plate_string[2:]), con, cursor, user_id)
            video_capture.release()
            cv2.destroyAllWindows()
            return c_plate_string
        except: pass


def detect_license_plate(image):
    """Resizing image to width of 500 and creating a copy of that."""
    resize = imutils.resize(image, width=500)
    image_copy = resize.copy()
    # cv2.imshow("Resize Scale", resize)

    """Conervting the image into gray."""
    gray = cv2.cvtColor(resize, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("Grey Scale", gray)

    """Removing extra noise if present in the iamge."""
    noise_removal = cv2.bilateralFilter(gray, 11, 17, 17)
    # cv2.imshow('Noise Removal', noise_removal)

    """Detecting out the edges present inside the image."""
    canny = cv2.Canny(noise_removal, 170, 200)
    # cv2.imshow("Canny image", canny)

    """Finding contours region present in the image."""
    contours, _ = cv2.findContours(canny.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    contour = sorted(contours, key=cv2.contourArea, reverse=True)[:30]
    NumberPlateCnt, X, Y, W, H = None, None, None, None, None
    for cnts in contour:
        peri = cv2.arcLength(cnts, True)
        approx = cv2.approxPolyDP(cnts, 0.02 * peri, True)
        """Select the contour with 4 corners"""
        if len(approx) == 4:
            NumberPlateCnt = approx
            X, Y, W, H = cv2.boundingRect(cnts)
            # print(X, Y, W, H)
            break

    """Drawing contour on the approx region."""
    cv2.drawContours(resize, [NumberPlateCnt], -1, (0, 255, 0), 3)
    # roi_contour = image_copy[Y: Y+H, X: X+W]
    # cv2.imshow("Check for contour", roi_contour)

    """Cropping the required region of interest(ROI) to get the only License Plate Section."""
    roi = image_copy[Y: Y+H, X: X+W]
    # cv2.imshow("Final Image With Number Plate Detected", roi)
    # cv2.waitKey(0)
    return characterrecognition(roi)


def characterrecognition(roi):
    """Conervting the image into gray."""
    gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("Grey Scale for number detection", gray)

    """Removing extra noise if present in the iamge."""
    noise_removal = cv2.bilateralFilter(gray, 11, 17, 17)
    # cv2.imshow('Noise Removal for number detection', noise_removal)

    """Detecting out the edges present inside the image."""
    thresh_inv = cv2.adaptiveThreshold(noise_removal, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 39, 1)
    canny = cv2.Canny(thresh_inv, 170, 200)
    # cv2.imshow("Canny image", canny)

    """Finding contours region present in the image."""
    (cnts, _) = cv2.findContours(canny.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    # cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    cnts = sorted(cnts, key=lambda cnts: cv2.boundingRect(cnts)[0], reverse=True)
    roi_area = roi.shape[0] * roi.shape[1]
    NumberPlateCnt, X, Y, W, H, x = None, None, None, None, None, []
    for c in cnts:
        X, Y, W, H = cv2.boundingRect(c)
        area = W * H
        non_max_sup = area / roi_area
        # cv2.rectangle(roi, (X, Y), (X + W, Y + H), (0, 255, 0), 2)
        # new_roi = roi[Y: Y+H, X: X+W]
        # cv2.imshow("{}".format(i+1), new_roi)
        if 0.015 <= non_max_sup < 0.09:
            # print(i+1, area, non_max_sup)
            cv2.rectangle(roi, (X, Y), (X + W, Y + H), (0, 255, 0), 2)
            char = gray[Y: Y + H, X: X + W]
            binary = char < threshold_otsu(char)
            resized_char = resize(binary, (20, 20))
            x.append([resized_char.reshape(-1)])
    # cv2.imshow("Counter for text detection", roi)
    # cv2.waitKey(1)
    return character_segmentation(x)


def character_segmentation(x):
    plate = ""
    model = pickle.load(open("./models/SVM_model.sav", 'rb'))
    for each_character in range(0, len(x), 2):
        new_character = x[each_character][0].reshape(1, -1)
        result = model.predict(new_character)
        plate += " ".join(result[0])
        # print(each_character)
    # print("License Number Plate ", plate[::-1])
    return plate[::-1]


def string_correction(rest):
    if rest[1] == "A":
        rest = rest.replace(rest[1], "4")
    elif rest[1] == "D":
        rest = rest.replace(rest[1], "0")
    elif rest[1] == "Z":
        rest = rest.replace(rest[1], "2")
    else:
        pass
    if rest[2] == "0":
        rest = rest.replace(rest[2], "D")
    elif rest[2] == "4":
        rest = rest.replace(rest[2], "A")
    elif rest[2] == "2":
        rest = rest.replace(rest[2], "z")
    else:
        pass
    return rest


def database(plate, con, cursor, user_id):
    try:
        # query = "UPDATE `in_mall` SET `status` = 1 WHERE `user_id` = {}".format(user_id)
        query = "UPDATE `in_mall` SET `status` = 1 WHERE `user_id` = {} AND `licence_plate` = '{}'".format(user_id, plate)
        cursor.execute(query)
        con.commit()
    except Exception as e:
        print("Exception occured -", e)


# main(1)